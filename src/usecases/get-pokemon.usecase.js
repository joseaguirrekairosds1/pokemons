
import { Pokemon } from "../model/pokemon";
import { PokemonRepository } from "../repositories/pokemon.repository";

export class GetPokemonUseCase {
  static async execute(id) {
    const repository = new PokemonRepository();
    const responseApi = await repository.getPokemon(id);
    const pokemon = new Pokemon(responseApi);

    return pokemon || {};
  }
}
