import { TypePokemon } from "../model/type-pokemon";
import { PokemonRepository } from "../repositories/pokemon.repository";

export class TypeListUseCase {
  static async execute() {
    const repository = new PokemonRepository();
    const responseApi = await repository.getListType();
    const pokemonList =
      responseApi.results
        .map(type => new TypePokemon(type))
        .filter(type => type.name !== "Unknown" && type.name !== "Shadow");

    return pokemonList || [];
  }
}
