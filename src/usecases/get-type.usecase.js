import { TypePokemon } from "../model/type-pokemon";
import { PokemonRepository } from "../repositories/pokemon.repository";

export class GetTypeUseCase {
  static async execute(id) {
    const repository = new PokemonRepository();
    const responseApi = await repository.getType(id);
    const type = new TypePokemon(responseApi);

    return type || {};
  }
}
