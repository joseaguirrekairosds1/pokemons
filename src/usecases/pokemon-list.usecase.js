
import { Pokemon } from "../model/pokemon";
import { PokemonRepository } from "../repositories/pokemon.repository";

export class PokemonListUseCase {
  static async execute() {
    const repository = new PokemonRepository();
    const responseApi = await repository.getListPokemons();
    const pokemonList = responseApi.results.map(poke => new Pokemon(poke));

    return pokemonList || [];
  }
}
