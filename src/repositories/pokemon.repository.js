import axios from "axios";

export class PokemonRepository {
  async getListPokemons() {
    return (
      await axios.get(`https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0`)
    ).data;
  }

  async getPokemon(id) {
    return (
      await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
    ).data;
  }

  async getListType() {
    return (
      await axios.get(`https://pokeapi.co/api/v2/type`)
    ).data;
  }

  async getType(id) {
    return (
      await axios.get(`https://pokeapi.co/api/v2/type/${id}`)
    ).data;
  }
}
