import { LitElement, html, css } from "lit";
import { GetTypeUseCase } from "../usecases/get-type.usecase";
import "./../ui/pokemon.ui";
import { PokemonListUseCase } from "../usecases/pokemon-list.usecase";

export class PokemonList extends LitElement {

  static get styles() {
    return css`
      :host {
        display: flex;
        border: black solid 2px;
        border-radius: 0.5rem;
        margin: 0px 0.5rem 0.5rem 0.5rem; 
        flex-wrap: wrap;
        flex-direction: row;
        justify-content: space-evenly;
        height: 100%;
        overflow-y: auto;
      }
      .form {
        margin: 1rem;
      }
      label {
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-weight: 600;
      }
      input[type=text], textarea {
        width: 100%;
        padding: 12px 20px;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 1rem;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
      }

      .buttons-container {
        display: flex;
        background: none;
        padding: 0.5rem;
      }

      .button {
        display: inline-block;
        text-align: center;
        text-decoration: none;
        font-size: 1rem;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        width: 80%;
        background-color: #d17232;
        color: white;
        padding: 0.5rem;
        margin: 0.5rem;
        border: none;
        border-radius: 4px;
        cursor: pointer;
      }

      .button:hover {
        background-color: #c55e19;
      }

      div {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
      }
    `;
  }

  constructor() {
    super();
    this.pokemons = [];
  }

  static get properties() {
    return {
      pokemons: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    const id = Number(location.pathname.split("/").pop());
    if (id > 0) {
      const type = await GetTypeUseCase.execute(id);
      this.pokemons = type.pokemon;
    } else {
      this.pokemons = await PokemonListUseCase.execute(id);
    }
  }

  render() {
    return html`
    ${this.pokemons?.map(
      (pokemon) => html`<pokemon-ui .pokemon="${pokemon}"></pokemon-ui>`
    )}
    `;
  }
}

customElements.define("pokemon-list", PokemonList);
