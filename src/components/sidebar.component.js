import { LitElement, html, css } from "lit";
import { TypeListUseCase } from "../usecases/type-list.usecase";
import "./../ui/type.ui";

export class SidebarComponent extends LitElement {

  constructor() {
    super();
    this.types = [];
    this.typeSelected = {};
  }

  static get styles() {
    return css`
      :host {
        display: block;
        border: black solid 2px;
        border-radius: 0.5rem;
        margin: 0px 0.5rem 0.5rem 0.5rem;
        width: 100%;
        background-color: rgb(227 227 227 / 70%);
      }
      ul {
        padding: 0px;
      }
    `;
  }

  static get properties() {
    return {
      types: { type: Array },
      typeSelected: { type: Object }
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.types = await TypeListUseCase.execute();
    this.types.unshift({ id: 0, name: "Todos" });
  }

  render() {
    return html`
        <nav><ul>
          ${this.types?.map((type) => html`
              <li @click="${() => this.typeSelected = type}">
                <type-ui .selected="${this.typeSelected.id}" .type="${type}"></type-ui>
              </li>
          `)}
        </ul></nav>
    `;
  }
}

customElements.define("sidebar-menu", SidebarComponent);
