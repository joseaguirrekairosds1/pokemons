import { LitElement, html, css } from "lit";
import logoPokemon from '../assets/pokemon_logo.png';

export class HeaderComponent extends LitElement {
  constructor() {
    super();
  }

  static get styles() {
    return css`
      :host {
        display: block;
        border: black solid 2px;
        border-radius: 0.5rem;
        background-color: #e6e6e6;
      }
      header {
        margin: 0.5rem;
      }
      header a img  {
        height: 4rem;
      }
    `;
  }

  render() {
    return html`
        <header>
          <a href="/">
            <img .src="${logoPokemon}" alt="Pokemon Logo">
          </a>
        </header>
    `;
  }
}

customElements.define("header-pokemon", HeaderComponent);
