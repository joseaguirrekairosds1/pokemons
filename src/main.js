import "./main.css";
import "./components/header.component";
import "./components/sidebar.component";
import { Router } from "@vaadin/router";

import "./pages/home.page";
import "./pages/type.page";

const router = new Router(document.getElementById('outlet'));

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/type/0", redirect: "/" },
  { path: "/(type[s]?)/:id", component: "type-page" },
  { path: "(.*)", redirect: "/" },
]);
