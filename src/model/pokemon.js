export class Pokemon {
  constructor({ name, url, id, types, abilities, height, weight }) {
    this.name = name.charAt(0).toUpperCase() + name.slice(1);
    this.url = url || '';
    this.id = id || Number(url.split("/")[6]);
    this.types = types || [];
    this.abilities = abilities || [];
    this.height = height || '';
    this.weight = weight || '';
  }
}
