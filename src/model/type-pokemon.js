import { Pokemon } from "./pokemon";

export class TypePokemon {
  constructor({ name, url, pokemon, id }) {
    this.name = name.charAt(0).toUpperCase() + name.slice(1);
    this.url = url || '';
    this.id = id || Number(url.split("/")[6]);
    this.pokemon = (pokemon?.map((poke) => {
      return new Pokemon(poke.pokemon);
    })) || [];
  }
}
