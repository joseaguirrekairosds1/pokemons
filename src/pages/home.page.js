import "../components/pokemon-list.component";

export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <pokemon-list></pokemon-list>
    `;
  }
}

customElements.define("home-page", HomePage);
