import "../components/pokemon-list.component";

export class TypePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <pokemon-list></pokemon-list>
    `;
  }
}

customElements.define("type-page", TypePage);
