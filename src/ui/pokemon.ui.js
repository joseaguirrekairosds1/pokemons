import { LitElement, html, css } from "lit";

export class PokemonUI extends LitElement {

  constructor() {
    super();
    this.type = {};
  }

  static get styles() {
    return css`
      :host{
        display: block;
        width: 12rem;
        margin: 0.5rem;
      }
      .card {
        display: block;
        text-align: center;
        background-color: rgba(77, 77, 77,80%);
        border: black solid 1px;
        padding: 1rem;
        border-radius: 4px;
        width: 10rem;
        height: 8rem;
      }
      .card img {
        display: inline-block;
        width: 6rem;

      }
      .card label {
        display: block;
        font-size: 1rem;
        color: white;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
      }
  `;
  }

  static get properties() {
    return {
      pokemon: { type: Object }
    };
  }

  render() {
    return html`
      <div class="card">
        <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${this.pokemon.id}.png" alt="${this.pokemon.name}">
        <label>${this.pokemon.id} - ${this.pokemon.name}</label>        
      </div>
    `;
  }
}

customElements.define("pokemon-ui", PokemonUI);
