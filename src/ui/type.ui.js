import { LitElement, html, css } from "lit";
import { TYPE_COLORS } from "../enums/type-colors.enum";

export class TypeUI extends LitElement {

  constructor() {
    super();
    this.type = {};
    this.selected = '';
  }

  static get styles() {
    return css`
      :host{
        display: block;
        max-width: 100%;
        margin: 0.5rem 1rem 1rem 0.5rem;
      }
      .button {
          display: inline-block;
          text-align: left;
          text-decoration: none;
          font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
          font-size: 1rem;
          width: 95%;
          padding: 0.4rem;
          margin: 0px;
          border: none;
          border-radius: 4px;
          color: white;

          cursor: pointer;
      }
      .button:hover:not(.type-selected) {
        opacity: 0.7;
      }
      .type-selected {
        border: black solid 3px;
      }
  `;
  }

  static get properties() {
    return {
      type: { type: Object },
      selected: { type: String }
    };
  }

  render() {
    return html`
      <a
        style="background-color: ${TYPE_COLORS[this.type.id]};"
        class="button ${this.selected === this.type.id ? 'type-selected' : ''}"
        .href="/type/${this.type.id}">${this.type.name}</a>
    `;
  }
}

customElements.define("type-ui", TypeUI);
