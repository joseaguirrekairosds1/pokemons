import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { TypeListUseCase } from "../src/usecases/type-list.usecase";
import { TYPE_LIST_RESPONSE } from "./fixtures/pokemon-response";

jest.mock("../src/repositories/pokemon.repository");

describe("Get list of pokemon tpyes", () => {
  beforeEach(() => {
    PokemonRepository.mockClear();
  });

  it("should execute properly", async () => {
    PokemonRepository.mockImplementation(() => {
      return {
        getListType: () => TYPE_LIST_RESPONSE,
      };
    });

    const pokemonList = await TypeListUseCase.execute();

    expect(pokemonList.length).toBe(18);
    expect(pokemonList[2].name).toBe("Flying");
    expect(pokemonList[2].url).toBe("https://pokeapi.co/api/v2/type/3/");
  });
});
