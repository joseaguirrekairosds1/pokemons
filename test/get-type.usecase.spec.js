import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { GetTypeUseCase } from "../src/usecases/get-type.usecase";
import { TYPE_RESPONSE } from "./fixtures/pokemon-response";


jest.mock("../src/repositories/pokemon.repository");

describe("Get type", () => {
  it("should execute properly", async () => {
    PokemonRepository.mockImplementation(() => {
      return {
        getType: (id) => TYPE_RESPONSE,
      };
    });

    const pokemon = await GetTypeUseCase.execute(4);

    expect(pokemon.id).toBe(4);
    expect(pokemon.name).toBe("Poison");
    expect(pokemon.pokemon.length).toBe(101);
  });
});
