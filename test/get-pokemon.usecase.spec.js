import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { GetPokemonUseCase } from "../src/usecases/get-pokemon.usecase";
import { POKEMON_RESPONSE } from "./fixtures/pokemon-response";


jest.mock("../src/repositories/pokemon.repository");

describe("Get pokemon", () => {
  it("should execute properly", async () => {
    PokemonRepository.mockImplementation(() => {
      return {
        getPokemon: (id) => POKEMON_RESPONSE,
      };
    });

    const pokemon = await GetPokemonUseCase.execute(5);

    expect(pokemon.id).toBe(5);
    expect(pokemon.name).toBe("Charmeleon");
    expect(pokemon.weight).toBe(190);
  });
});
