import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { PokemonListUseCase } from "../src/usecases/pokemon-list.usecase";
import { LIST_RESPONSE } from "./fixtures/pokemon-response";


jest.mock("../src/repositories/pokemon.repository");

describe("Get list of pokemons", () => {
  beforeEach(() => {
    PokemonRepository.mockClear();
  });

  it("should execute properly", async () => {
    PokemonRepository.mockImplementation(() => {
      return {
        getListPokemons: (limit, offset) => LIST_RESPONSE,
      };
    });

    const pokemonList = await PokemonListUseCase.execute(5, 0);

    expect(pokemonList.length).toBe(50);
    expect(pokemonList[1].name).toBe("Ivysaur");
    expect(pokemonList[1].url).toBe("https://pokeapi.co/api/v2/pokemon/2/");
  });
});
