describe('sidebar menu spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');

    cy.wait(1500);

    cy.get('sidebar-menu')
      .shadow()
      .find('nav ul li')
      .first()
      .find('type-ui')
      .shadow()
      .find('a')
      .should('have.text', 'Todos')
      .end();

    cy.get('sidebar-menu')
      .shadow()
      .find('nav ul li')
      .eq(4)
      .find('type-ui')
      .shadow()
      .find('a')
      .should('have.text', 'Poison')
      .end();
  })
})