describe('get fire pokemons spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');

    cy.wait(1500);

    cy.get('sidebar-menu')
      .shadow()
      .find('nav ul li')
      .eq(10)
      .click({ position: 'top' })
      .end();

    cy.wait(1500);

    cy.get('pokemon-list')
      .shadow()
      .find('pokemon-ui')
      .eq(2)
      .shadow()
      .find('.card')
      .find('label')
      .should('have.text', '6 - Charizard')
      .end();
  })
})