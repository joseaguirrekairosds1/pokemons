describe('get pokemon spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');

    cy.wait(1500);

    cy.get('pokemon-list')
      .shadow()
      .find('pokemon-ui')
      .eq(7)
      .shadow()
      .find('.card')
      .find('label')
      .should('have.text', '8 - Wartortle')
      .end();
  })
})